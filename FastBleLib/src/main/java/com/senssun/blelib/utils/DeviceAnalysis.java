package com.senssun.blelib.utils;

public final class DeviceAnalysis {
    public enum DeviceType {
        NullScale(-1, ""),
        SportScale(11, "moving-id101-id107-iw5913b-iw5915b"),
        CloudScale(12, "senssun cloud-ib_a2-if_b2-if_b6-if_b2a-if_b6a-if_b7-if_b7a senssun fat ib_a3"),
        WiFiFatScale(13, ""),

        BodyBroadScale(1, "ifit scale-senssun body"),
        BodyScale(7, ""),
        FatScale(2, "ble to uart_2-senssun fat-jointown"),
        FoodScale(3, "senssun food"),
        GrowthScale(4, "senssun growth-aguscales"),
        FatAlarmScale(5, "senssun fat_a"),
        EightBodyScale(6, "senssun fat pro"),
        BodyAlarmScale(8, "senssun body_a"),
        Eqi99Scale(9, "eqi-99"),
        Eqi912Scale(10, "eqi-912"),

        BleWeightScale(0, "IB_A2"),
        BleFatScaleDC(1, "SENSSUN CLOUD IF_B2 IF_B6"),
        BleFatScaleAC(2, "IF_B2A IF_B6A  senssun fat"),

        BroadWeightScale(3, "IB_A3 SENSSUN BODY_A"),
        BroadFatScaleAC(4, "IF_B7A"),
        BroadFatScaleDC(5, "IF_B7"),
        BroadBodyScale(6, "IB_A3"),

        QihooScale(18, "360"),
        BleFatSuperScale(19, "SENSSUN FAT_S SENSSUN FAT PRO  B8A"),
        BleFatSuperScale2(21, "IF_B8A WANBU CW618"),
        BleFatHeartScale(20, "SENSSUN Heart");

        public String BroadCasterName;
        public int TypeIndex;

        DeviceType() {
        }

        DeviceType(int TypeIndex, String BroadCasterName) {
            this.TypeIndex = TypeIndex;
            this.BroadCasterName = BroadCasterName;
        }
    }

    public enum DeviceTypeMode {
        BLE_Band_Scale(DeviceType.SportScale),
        SS_BLE_Scale(DeviceType.CloudScale), SS_BLE_Scale_DC(DeviceType.CloudScale), SS_BLE_Scale_AC(DeviceType.CloudScale), SS_BLE_Scale_AC_XH(DeviceType.CloudScale), SS_BLE_Scale_AC_JHW(DeviceType.CloudScale),
        SS_BroadCast_Scale_DC(DeviceType.CloudScale), SS_BroadCast_Scale_AC(DeviceType.CloudScale),
        SS_BLE_Super_fat_Scale2(DeviceType.CloudScale),
        SS_WIFI_Scale(DeviceType.WiFiFatScale), MD_WIFI_Scale(DeviceType.WiFiFatScale),
        SS_BroadCast_Scale_NO_Fat(DeviceType.CloudScale),
        Noidentify(DeviceType.NullScale);

        public DeviceType DataType;

        DeviceTypeMode(DeviceType DataType) {
            this.DataType = DataType;
        }
    }

    public static DeviceTypeMode GetDeviceForVendorAndModel(String vendorID, String modelID) {
        switch (vendorID) {
            case "0001":
            case "0002":
            case "0003":
            case "0004":
            case "01A8":
                switch (modelID) {
                    case "0201":
                    case "2117":
                    case "2118":
                    case "0224":
                        return DeviceTypeMode.SS_BLE_Scale;    //只有测量体重的称
                    case "0302":
                    case "0301":
                    case "0113":
                    case "0115":
                    case "0116":
                    case "0225":
                        return DeviceTypeMode.SS_BLE_Scale_DC;
                    case "0303":
                    case "0304":
                    case "0126":// 2018/10/11
                        return DeviceTypeMode.SS_BLE_Scale_AC;
                    case "1115":
                        return DeviceTypeMode.SS_BLE_Scale_AC_JHW;
                    case "3116":
                        return DeviceTypeMode.SS_BLE_Scale_AC_XH;
                    case "0311":
                    case "0313":
                    case "1D07":
                    case "7431":
                        return DeviceTypeMode.SS_BroadCast_Scale_DC;
                    case "0312":
                        return DeviceTypeMode.SS_BroadCast_Scale_AC;
                    case "0122":
                        return DeviceTypeMode.SS_WIFI_Scale;
                    case "1739":
                    case "1738":
                        return DeviceTypeMode.BLE_Band_Scale;
                    case "0202":
                        return DeviceTypeMode.SS_BroadCast_Scale_NO_Fat;
                    case "0308":
                        return DeviceTypeMode.SS_BLE_Super_fat_Scale2;
                    default:
                        return DeviceTypeMode.Noidentify;
                }
            case "1728":
                switch (modelID) {
                    case "0305":
                        return DeviceTypeMode.MD_WIFI_Scale;
                    default:
                        return DeviceTypeMode.Noidentify;
                }
            default:
                return DeviceTypeMode.Noidentify;
        }
    }

}
