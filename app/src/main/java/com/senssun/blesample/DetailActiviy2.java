package com.senssun.blesample;

import android.app.Activity;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.senssun.blelib.BleManager;
import com.senssun.blelib.callback.BleGattCallback;
import com.senssun.blelib.callback.BleReadCallback;
import com.senssun.blelib.data.BleDevice;
import com.senssun.blelib.exception.BleException;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActiviy2 extends Activity {
    private static final String TAG = "DetailActiviy2";
    BleDevice bleDevice;
    Handler handler = new Handler();
    @BindView(R.id.et_count)
    EditText etCount;
    @BindView(R.id.btn)
    Button btn;
    @BindView(R.id.tv_content)
    TextView tvContent;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail);
        ButterKnife.bind(this);
        bleDevice = getIntent().getParcelableExtra("device");
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start();
            }
        });
    }

    int count = 100;
    int test = 100;

    void start() {
//        count = TextUtils.isEmpty(etCount.getText().toString()) ? 100 : Integer.valueOf(etCount.getText().toString().trim());
//        test = count;
        BleManager.getInstance().setReConnectCount(0);
        doTest();
    }

    BluetoothGattCharacteristic read, write;
    BluetoothGattService mainService;

    void doTest() {
        count--;
        BleManager.getInstance().connect(bleDevice, new BleGattCallback() {
            @Override
            public void onStartConnect() {
                bleDevice.setTimestampNanos(System.currentTimeMillis());
            }

            @Override
            public void onConnectFail(BleDevice bleDevice, BleException exception) {
                if (count > 0) {
                    next();
                }
            }

            @Override
            public void onConnectSuccess(BleDevice bleDevice, BluetoothGatt gatt, int status) {
                List<BluetoothGattService> services = BleManager.getInstance().getBluetoothGattServices(bleDevice);
                for (BluetoothGattService bluetoothGattService : services) {
                    List<BluetoothGattCharacteristic> characteristics = bluetoothGattService.getCharacteristics();
                    for (BluetoothGattCharacteristic characteristic : characteristics) {
                        if (((characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE) != 0) || ((characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_WRITE) != 0)) {
                            write = characteristic;
                        }
                        if ((characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) != 0 || (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_INDICATE) > 0) {
                            read = characteristic;
                            mainService = bluetoothGattService;
                        }
                    }
                }
                BleManager.getInstance().read(bleDevice, mainService.getUuid().toString(), read.getUuid().toString(), new BleReadCallback() {
                    @Override
                    public void onReadSuccess(byte[] data) {
                        StringBuffer manuBuffer = new StringBuffer();
                        if (data != null) {
                            for (byte byteChar : data) {
                                String ms = String.format("%02X ", byteChar).trim();
                                manuBuffer.append(ms);
                            }
                        }
                        Log.d(TAG, "OnShow: " + manuBuffer.toString());
                    }

                    @Override
                    public void onReadFailure(BleException exception) {

                    }
                });
            }

            @Override
            public void onDisConnected(boolean isActiveDisConnected, BleDevice bleDevice, BluetoothGatt gatt, int status) {
                if (count > 0) {
                    next();
                }
            }
        });

    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            doTest();
        }
    };

    void next() {
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, 3000);
    }


}
