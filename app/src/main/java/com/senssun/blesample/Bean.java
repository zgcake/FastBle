package com.senssun.blesample;

public class Bean {
    float division, weight;
    int zuKang, unit;
    boolean ifStable;

    public Bean(float division, float weight, int zuKang, boolean ifStable, int unit) {
        this.division = division;
        this.weight = weight;
        this.zuKang = zuKang;
        this.unit = unit;
        this.ifStable = ifStable;
    }

    public float getDivision() {
        return division;
    }

    public void setDivision(float division) {
        this.division = division;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public int getZuKang() {
        return zuKang;
    }

    public void setZuKang(int zuKang) {
        this.zuKang = zuKang;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public boolean isIfStable() {
        return ifStable;
    }

    public void setIfStable(boolean ifStable) {
        this.ifStable = ifStable;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Bean) {
            Bean bean = (Bean) obj;
            return this.weight == bean.getWeight() && this.ifStable == bean.isIfStable() && this.zuKang == bean.zuKang;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Bean{" +
                "division=" + division +
                ", weight=" + weight +
                ", zuKang=" + zuKang +
                ", unit=" + unit +
                ", ifStable=" + ifStable +
                '}';
    }
}
