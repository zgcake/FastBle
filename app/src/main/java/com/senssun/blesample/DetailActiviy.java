package com.senssun.blesample;

import android.app.Activity;
import android.bluetooth.BluetoothGatt;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.senssun.blesample.bean.RecordBean;
import com.senssun.blelib.BleManager;
import com.senssun.blelib.callback.BleGattCallback;
import com.senssun.blelib.callback.BleRssiCallback;
import com.senssun.blelib.data.BleDevice;
import com.senssun.blelib.exception.BleException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActiviy extends Activity {
    BleDevice bleDevice;
    Handler handler = new Handler();
    @BindView(R.id.et_count)
    EditText etCount;
    @BindView(R.id.btn)
    Button btn;
    @BindView(R.id.tv_content)
    TextView tvContent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail);
        ButterKnife.bind(this);
        bleDevice = getIntent().getParcelableExtra("device");
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start();
            }
        });
    }

    int count = 100;
    int test = 100;

    void start() {
        count = TextUtils.isEmpty(etCount.getText().toString()) ? 100 : Integer.valueOf(etCount.getText().toString().trim());
        test = count;
        BleManager.getInstance().setReConnectCount(0);
        doTest();
    }

    void doTest() {
        count--;
        tvContent.setText("当前是进行:" + (test - count));
        BleManager.getInstance().connect(bleDevice, new BleGattCallback() {
            @Override
            public void onStartConnect() {
                bleDevice.setTimestampNanos(System.currentTimeMillis());
            }

            @Override
            public void onConnectFail(BleDevice bleDevice, BleException exception) {
                if (count > 0) {
                    next();
                }
            }

            @Override
            public void onConnectSuccess(BleDevice bleDevice, BluetoothGatt gatt, int status) {
                bleDevice.setTimestampNanos(System.currentTimeMillis() - bleDevice.getTimestampNanos());
                readRssi(bleDevice);
                BleManager.getInstance().disconnect(bleDevice);
            }

            @Override
            public void onDisConnected(boolean isActiveDisConnected, BleDevice bleDevice, BluetoothGatt gatt, int status) {
                if (count > 0) {
                    next();
                }
            }
        });
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            doTest();
        }
    };

    void next() {
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, 3000);
    }

    private static final String TAG = "DetailActiviy";

    private void readRssi(final BleDevice bleDevice) {
        BleManager.getInstance().readRssi(bleDevice, new BleRssiCallback() {
            @Override
            public void onRssiFailure(BleException exception) {
                Log.i(TAG, "onRssiFailure" + exception.toString());
            }

            @Override
            public void onRssiSuccess(int rssi) {
                RecordBean recordBean = new RecordBean();
                recordBean.setMac(bleDevice.getMac());
                recordBean.setName(bleDevice.getName());
                recordBean.setRssi(rssi);
                recordBean.setPhoneName(Build.MODEL);
                recordBean.setVersion(Build.VERSION.RELEASE);
                recordBean.setConnectTime(bleDevice.getTimestampNanos());
                recordBean.save();
                Log.i(TAG, "onRssiSuccess: " + rssi);
            }
        });
    }
}
