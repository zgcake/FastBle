package com.senssun.blesample.util;

public final class ConstantBLEWIFI {
    //功能码

    public static final String CHECK_STATUS = "cc01";//检查状态

    public static final String SET_WIFI_OTA = "cc02"; // 模块开始WiFi OTA
    public static final String SET_WIFI_CONFIG = "cc03";//配置WiFi账号密码（蓝牙）
    public static final String SET_SERVER_PORT = "cc05";//配置服务器IP和端口
    public static final String SET_DEVICE_ID = "cc07";// 配置设备ID


}
