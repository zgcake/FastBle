package com.senssun.blesample.comm;


import com.senssun.blelib.data.BleDevice;

public interface Observer {

    void disConnected(BleDevice bleDevice);
}
