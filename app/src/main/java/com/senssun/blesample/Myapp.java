package com.senssun.blesample;

import android.app.Application;

import org.litepal.LitePal;

public class Myapp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        LitePal.initialize(this);
    }
}
